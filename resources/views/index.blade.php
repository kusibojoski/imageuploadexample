<form method="POST" action="/upload" enctype="multipart/form-data">
    @csrf
    <input type="text" name="title">
    <input type="file" name="images[]" multiple>
    <input type="submit" value="Prikaci">
</form>


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
